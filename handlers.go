package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

func (u *UserManagement) RegisterHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	for _, u := range u.users {
		if u.Username == user.Username {
			http.Error(w, "User already exists", http.StatusBadRequest)
			return
		}
	}

	if len(user.Password) < 12 {
		http.Error(w, "Password must be atleast 12 characters", http.StatusBadRequest)
		return
	}

	if !strings.Contains(user.Password, "_") &&
		!strings.Contains(user.Password, "!") &&
		!strings.Contains(user.Password, "@") &&
		!strings.Contains(user.Password, "#") &&
		!strings.Contains(user.Password, "*") {
		http.Error(w, "Password must contain atleast one special characters", http.StatusBadRequest)
		return
	}

	newUser := User{
		ID:       len(u.users) + 1,
		Username: user.Username,
		Password: user.Password,
		Role:     user.Role,
	}

	u.users = append(u.users, newUser)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "User created successfully")

	err = u.WriteUsersToFile()
	if err != nil {
		http.Error(w, "Error writing users to file", http.StatusBadRequest)
	}
}

func (u *UserManagement) LoginHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		w.Write([]byte("Provide valid input"))
		return
	}

	for _, u := range u.users {
		if u.Username == user.Username {
			if u.Password != user.Password {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte("Passsword incorrect"))
				return
			} else {
				w.WriteHeader(http.StatusNoContent)
				fmt.Fprintf(w, "Login Successful")
				return
			}
		} else {
			http.Error(w, "User not found", http.StatusBadRequest)
			return
		}
	}
}

func (u *UserManagement) GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(u.users)
}

func (u *UserManagement) GetUserByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	userID, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, "Invalid user ID", http.StatusBadRequest)
		return
	}

	for _, user := range u.users {
		if user.ID == userID {
			json.NewEncoder(w).Encode(user)
			return

		}
	}
	http.Error(w, "User not found", http.StatusBadRequest)
}

func (u *UserManagement) viewDetails(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)
	Id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "Error converting to string", http.StatusBadRequest)
		return
	}

	for _, user := range u.users {
		if user.ID == Id {
			if user.Role == "Admin" {
				json.NewEncoder(w).Encode(u.users)
				return
			} else {
				json.NewEncoder(w).Encode(user)
				return
			}
		} else {
			http.Error(w, "Invalid user id", http.StatusBadRequest)
			return
		}
	}
}

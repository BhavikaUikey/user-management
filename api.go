package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	userManagement := NewUserManagement("users.json")
	err := userManagement.ReadUsersFromFile()
	if err != nil {
		log.Fatal(err)
	}

	router := mux.NewRouter()

	router.HandleFunc("/register", userManagement.RegisterHandler).Methods("POST")
	router.HandleFunc("/login", userManagement.LoginHandler).Methods("POST")
	router.HandleFunc("/users", userManagement.GetUsers).Methods("GET")
	router.HandleFunc("/users/{id}", userManagement.GetUserByID).Methods("GET")
	router.HandleFunc("/view/{id}", userManagement.viewDetails).Methods("GET")

	http.Handle("/", router)
	fmt.Println("Listening on 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}

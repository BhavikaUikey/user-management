package main

import (
	"encoding/json"
	"log"
	"os"

	"golang.org/x/crypto/bcrypt"
)

type UserManagement struct {
	users    []User
	Filepath string
}

type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

func NewUserManagement(filepath string) *UserManagement {
	return &UserManagement{
		Filepath: filepath,
	}
}

func (u *UserManagement) WriteUsersToFile() error {
	data, err := json.MarshalIndent(u.users, "", "    ")
	if err != nil {
		return err
	}

	err = os.WriteFile(u.Filepath, data, 0644) //6: read&write to owner,4 only read
	if err != nil {
		return err
	}
	return nil
}

func (u *UserManagement) ReadUsersFromFile() error {
	if _, err := os.Stat(u.Filepath); os.IsNotExist(err) {
		return nil
	}

	file, err := os.ReadFile(u.Filepath)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(file, &u.users)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}
